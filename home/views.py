from django.shortcuts import render

# Create your views here.
def home(request):
	return render(request, 'Home.html')

def gallery(request):
	return render(request, 'music-gallery.html')
